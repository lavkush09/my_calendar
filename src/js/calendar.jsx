import React, { Component } from "react";
import Calendar from "react-calendar";
import "../css/calender.css";
import { connect } from "react-redux";
import { stat } from "fs";

class MyApp extends Component {
  state = {
    date: new Date(),
    rawData: {},
    currentData: {},
    cl_date: ""
  };

  onChange = date => {
    this.setState({
      date,
      cl_date: `${date.getFullYear()}-${date.getMonth() + 1}-${
        date.getDate() < 10 ? `0${date.getDate()}` : date.getDate()
      }`
    });
  };

  componentDidMount() {
    let url =
      "http://api.openweathermap.org/data/2.5/forecast?q=Bengaluru,IND&APPID=5deb7dfdae8d91e9c560eb3d65a4acaa";
    fetch(url)
      .then(res => res.json())
      .then(data => {
        this.setState({ rawData: data });
      })
      .catch(err => console.log(err));
  }

  componentDidUpdate(preProps, preState) {
    let { date, cl_date } = this.state;

    if (preProps.cl_date !== cl_date) {
      let temData = this.state.rawData.list.filter(
        (item, i) => item.dt_txt.split(" ")[0] === cl_date
      );
      this.props.dispatch({
        type: "OPEN_WEATHER_API_DATA",
        data: temData
      });
    }

    if (preState.date !== date) {
      fetch(`http://interview.com360degree.com/api/getWeather?date=${date}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(res => res.json())
        .then(data => {
          let tList = data.data[0].forecast.filter(
            item => item.date === this.state.cl_date
          );
          if (tList.length > 0) {
            this.setState({ currentData: tList[0] });
            this.props.dispatch({
              type: "WEATHER_DATA",
              data: tList[0]
            });
          }
        })
        .catch(err => console.log(err));
    }
  }

  render() {
    let { currentData } = this.props;
    return (
      <div className="row m-0">
        <div className="col-lg-8 p-2 mx-auto">
          <div className="card">
            <div className="card-body">
              <Calendar
                onChange={this.onChange}
                value={this.state.date}
                className="cal_width"
              />
            </div>
          </div>
          <div className="d-flex">
            <div className="card height_wi_style mr-2">
              <div className="card-header">OpenWeather API Details</div>
            </div>
            {Object.keys(currentData).length > 0 && (
              <div className="card height_wi_style ml-2">
                <div className="card-header">Stud API Details</div>
                <div className="card-body">
                  <div className="card m-1">
                    <div className="card-body p-2 text-center">{`Date: ${currentData.date}`}</div>
                  </div>
                  <div className="card m-1">
                    <div className="card-body p-2 text-center">{`Day: ${currentData.day}`}</div>
                  </div>
                  <div className="card m-1">
                    <div className="card-body p-2 text-center">{`Humidity: ${currentData.precip}`}</div>
                  </div>
                  <div className="card m-1">
                    <div className="card-body p-2 text-center">{`Sky: ${currentData.skytextday}`}</div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({ currentData: state.rawData });
export default connect(mapStateToProps)(MyApp);
