const initialState = {
  rawData: {},
  openWeatherData: []
};

const rootStore = (state = initialState, action) => {
  switch (action.type) {
    case "":
      return {
        ...state
      };
    case "WEATHER_DATA":
      return { ...state, rawData: action.data };
    case "OPEN_WEATHER_API_DATA":
      return {
        ...state,
        openWeatherData: action.data
      };
    default: {
      return {
        ...state
      };
    }
  }
};

export default rootStore;
